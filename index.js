(function () {
  const shadowDOM = document.createElement('div');
  shadowDOM.attachShadow({mode: 'open'});
  const widget = document.createElement('div');
  widget.className = "widget";
  document.body.append(widget);

  const widgetStyles = {
    'display': 'flex',
    'flex-direction': 'column',
    'align-items': 'center',
    'justify-content': 'space-evenly',
    'background-color': 'grey',
    'width': '350px',
    'height': '200px',
    'border': '1px solid blue',
    'position': 'fixed',
    'top': 0,
    'cursor': 'move'
  };

  Object.assign(widget.style, widgetStyles);

  widget.innerHTML = `
    <span style="position: absolute; cursor: pointer; top: 5px; right: 5px;" class="button__close">X</span>
    <h1 class='widget__title'>Search node element</h1>
    <form>
      <input class="input__search" type="text" name="search" value="">
      <input class="button__search" type="submit" name="submit" value="Search" style="cursor: pointer;">
    </form>
    <h2 class="found__tag"></h2>
    <div class="button" style="width: 100%; display: flex; margin-bottom: 10px">
      <button class="button__prev">Prev</button>
      <button class="button__next">Next</button>
      <button class="button__parent">Parent</button>
      <button class="button__child">Child</button>
    </div>`

  const buttonPrev = widget.querySelector('.button__prev');
  const buttonNext = widget.querySelector('.button__next');
  const buttonParent = widget.querySelector('.button__parent');
  const buttonChild = widget.querySelector('.button__child');
  const buttonSearch = widget.querySelector('.button__search');
  const inputSearch = widget.querySelector('.input__search');
  const buttons = widget.querySelectorAll('button');
  const buttonClose = widget.querySelector('.button__close');

  shadowDOM.appendChild(widget);
  shadowDOM.shadowRoot.appendChild(widget);
  document.body.prepend(shadowDOM);


  buttons.forEach(elem => {
    elem.className = 'button__disabled'
    elem.setAttribute("style", "width: 25%; height: 30px; cursor: pointer;")
    elem.setAttribute("disabled", "disabled")
  });

  function handlerInput(event) {
    inputSearch.setAttribute("value", `${event.target.value}`)
  };

  let inputValue;
  let foundTag;

  function onButtonSearch(event) {
    event.preventDefault()
    inputValue = inputSearch.value;
    foundTag = inputValue ? document.querySelectorAll(inputValue)[0] : ""
    if (foundTag) {
      foundTag.style.border = '1px solid red';
      setButtonsStatus();
    } else showWorning();
  };

  function showWorning() {
    alert('Your tag doesn`t exist!');
  };

  function setButtonsStatus() {
    if (foundTag.firstElementChild) {
      buttonChild.disabled = false
    } else {
      buttonChild.disabled = true
    }
    if (foundTag.parentElement) {
      buttonParent.disabled = false
    } else {
      buttonParent.disabled = true
    }
    if (foundTag.previousElementSibling) {
      buttonPrev.disabled = false
    } else {
      buttonPrev.disabled = true
    }
    if (foundTag.nextElementSibling) {
      buttonNext.disabled = false
    } else {
      buttonNext.disabled = true
    }
  };

  let currentTag = null;
  let prevTag = null;

  function setProperties(props) {
    prevTag = foundTag;
    prevTag.style.border = 'none';
    currentTag = props;
    currentTag.style.border = '1px solid red';
    currentTag.scrollIntoView({block: 'center'});
    foundTag = currentTag;
    inputSearch.value = currentTag.tagName;
  }


  function onButtonNext() {
    if (foundTag.nextElementSibling) {
      setProperties(foundTag.nextElementSibling)
    }
    setButtonsStatus();
  };

  function onButtonPrev() {
    if (foundTag.previousElementSibling) {
      setProperties(foundTag.previousElementSibling)
    }
    setButtonsStatus();
  };

  function onButtonParent() {
    if (foundTag.parentElement) {
      setProperties(foundTag.parentElement)
    }
    setButtonsStatus();
  };

  function onButtonChild() {
    if (foundTag.children) {
      setProperties(foundTag.firstElementChild)
    }
    setButtonsStatus();
  };

  inputSearch.addEventListener('input', handlerInput);
  buttonSearch.addEventListener('click', onButtonSearch);
  buttonNext.addEventListener('click', onButtonNext);
  buttonParent.addEventListener('click', onButtonParent);
  buttonPrev.addEventListener('click', onButtonPrev);
  buttonChild.addEventListener('click', onButtonChild);
  buttonClose.addEventListener('click', onButtonClose);

  function onButtonClose() {
    inputSearch.removeEventListener('input', handlerInput);
    buttonSearch.removeEventListener('click', onButtonSearch);
    buttonNext.removeEventListener('click', onButtonNext);
    buttonParent.removeEventListener('click', onButtonParent);
    buttonPrev.removeEventListener('click', onButtonPrev);
    buttonChild.removeEventListener('click', onButtonChild);
    shadowDOM.remove();
  } 

  function dragElement(elmnt) {
    let X = 0, Y = 0, x = 0, y = 0;
    elmnt.onmousedown = dragMouseDown;

    function dragMouseDown(e) { 
        x = e.clientX;
        y = e.clientY;
        document.onmouseup = closeDragElement; 
        document.onmousemove = elementDrag;
    }

    function elementDrag(e) { 
        X = x - e.clientX;
        Y = y - e.clientY;
        x = e.clientX;
        y = e.clientY; 
        elmnt.style.top = (elmnt.offsetTop - Y) + "px";
        elmnt.style.left = (elmnt.offsetLeft - X) + "px";
    }

    function closeDragElement() { 
        document.onmouseup = null;
        document.onmousemove = null;
    }
}

dragElement(widget);

})()